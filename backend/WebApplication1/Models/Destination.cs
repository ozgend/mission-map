﻿namespace DataService.Models
{
    public class Destination
    {
        public string Address { get; set; }
        public string Title { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
    }
}